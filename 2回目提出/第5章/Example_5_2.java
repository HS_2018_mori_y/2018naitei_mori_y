public class Example_5_2 {
	public static void main(String[] args) {
		String title = "卒業式";
		String address = "sotugyousyukan@ac.jp";
		String text = "3月5日から卒業週間です";
		email(title, address, text);
	}
	public static void email(String title, String address, String text ) {
		System.out.println(address + "に、以下のメールを送信しました");
		System.out.println("件名：" + title);
		System.out.println("本文：" + text);
	}
}
