import java.util.Random;
public class Cleric_8_4 {
	String name;
	int hp = 50;
	final int MAX_HP = 50;
	int mp = 10;
	final int MAX_MP = 10;

	public void selfAid() {
	System.out.println(this.name + "はセルフエイドを唱えた！");
	this.hp = this.MAX_HP;
	this.mp -= 5;
		System.out.println("HPが最大まで回復した。");
	}

    public int pray(int sec) {
    	System.out.println(this.name + "は" + sec + "秒間天に祈った！");

        //論理上の回復量をランダムに用いて決定する
    	int recover = new Random().nextInt(3) +sec;

        //実際の回復量を計算する
    	int recoverActual = Math.min(this.MAX_HP - this.mp, recover);

    	this.mp += recoverActual;
    	System.out.println("MPが" + recoverActual + "回復した。" );
    	return recoverActual;
    }
}
