public class Example_4_4 {
	public static void main(String[] args) {
		// ①配列numbersの準備
		int[] numbers = { 3, 4, 9 };

		// ②メッセージの準備
		System.out.println("1桁の数字を入力してください");

		// ③キーボードから数字の入力
		int input = new java.util.Scanner(System.in).nextInt();

		// ④配列を回して判定
		for (int n : numbers) {
			if (n == input) {
				System.out.println("アタリ!");
			}
		}
	}
}
